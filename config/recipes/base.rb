def template(from, to) 
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

def create_dir dir 
  run "if ! [[ -d #{dir} ]]; then mkdir -p #{dir}; fi" 
end

namespace :deploy do

  task :create_extra_dirs do
    create_dir "#{shared_path}/config"
    create_dir "#{shared_path}/log"
    create_dir "#{shared_path}/pids"
    create_dir "#{shared_path}/unicorn"
  end
  after "deploy:create_symlink", "deploy:create_extra_dirs"

  task :setup_database_yml do
    template "database.erb", "#{shared_path}/config/database.yml"
  end 
  after "deploy:create_symlink", "deploy:setup_database_yml"

  task :link_db do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/unicorn #{release_path}/config/unicorn"
  end

  after "deploy:create_symlink", "deploy:link_db"
end
