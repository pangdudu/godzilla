Godzilla::Application.routes.draw do
  resources :resources
  resources :epics
  resources :products
  get 'product_roadmap' => 'roadmap#products'
  get 'product_roadmap_parallel' => 'roadmap#products_parallel'

  root :to => 'products#index'
end
