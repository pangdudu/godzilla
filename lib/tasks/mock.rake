namespace :mock do
  desc "Generate a backlog of mock data!!!"
  task :generate_backlog => :environment do
    Resource.create(title: "UX", capacity: 5.0, color: "#b89bc9") if Resource.where(title: "UX").blank?
    Resource.create(title: "PM", capacity: 7.5, color: "#68a6cf") if Resource.where(title: "PM").blank?
    Resource.create(title: "Backend", capacity: 15.0, color: "#9fc687") if Resource.where(title: "Backend").blank?
    Resource.create(title: "Android", capacity: 10.0, color: "#e9d080") if Resource.where(title: "Android").blank?
    Resource.create(title: "iOS", capacity: 10.0, color: "#df7a6e") if Resource.where(title: "iOS").blank?
    Resource.create(title: "QA", capacity: 5.0, color: "#817679") if Resource.where(title: "QA").blank?

    (3 + rand(20)).times do
      p = Product.new
      p.title = Faker::Company.catch_phrase
      p.priority = 1 + rand(20)
      p.vision = Faker::Lorem.paragraph(5 + rand(10))
      p.scope = Faker::Lorem.paragraph(2 + rand(10))
      features = []
      (2 + rand(3)).times { features << "* #{Faker::Company.bs}"}
      p.features = features.join("\n")

      p.save

      puts "Generated product:"
      ap p

      resources = []
      (1+rand(5)).times do
        resources << Resource.pluck(:id).sample
      end

      resources.uniq!

      (1 + rand(20)).times do
        e = p.epics.create
        e.priority = 1 + rand(20)
        e.title = Faker::Company.catch_phrase
        e.description = Faker::Lorem.paragraph(5 + rand(10))
        names = []
        (1 + rand(3)).times { names << "#{Faker::Name.name}"}
        e.stake_holders = names.join(",")
        e.save

        (1+rand(3)).times do
          e.epic_resources.create(resource_id: resources.sample, time_estimation: (1+ rand(40)))
        end

        puts "Generated epic:"
        ap e
      end
    end
  end
end
