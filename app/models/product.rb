class Product < ActiveRecord::Base
  attr_accessible :features, :priority, :scope, :title, :vision

  has_many :epics

  def estimate_for(resource)
    estimate = 0
    self.epics.each do |epic|
      estimate += epic.estimate_for(resource)
    end
    estimate
  end

  def longest_estimate
    longest_estimate = { resource: nil, estimate: 0 }
    Resource.all.each do |r|
      # divide estimate by IDM (capacity should be per)
      estimate = self.estimate_for(r)/r.capacity
      longest_estimate = {resource: r, estimate: estimate} if estimate > longest_estimate[:estimate]
    end
    return longest_estimate
  end

  def resources_for_pipeline
    resources = {}
    Resource.all.each do |r|
      resources[r] = (self.estimate_for(r)/r.capacity).to_i
    end
    return resources
  end
end
