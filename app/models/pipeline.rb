class Pipeline
  attr_accessor :pipeline, :swimlanes

  def initialize
    @pipeline = {}
    Resource.pluck(:id).each { |rid| @pipeline[rid] = [] }
    @swimlanes = []
  end
  
  def fill
    Product.order(:priority).each do |p|
      p.resources_for_pipeline.each do |resource, estimate|
        estimate.times do
          @pipeline[resource.id] << p.id
        end
      end
    end
  end

  def fill_swimlanes
    products = {}
    max = self.max_length

    (0..max).each do |i|
      @pipeline.each do |resource_id, pipe|
        if pipe.length > i
          product_id = pipe[i]
          products[product_id] = {start: max, end: 0} if !products.has_key?(product_id)
          products[product_id][:start] = i if products[product_id][:start] > i
          products[product_id][:end] = i if products[product_id][:end] < i
        end
      end
    end
    
    lanes = []
    products.each do |product_id, range|
      lane = 0
      while next_lane(lanes, lane, product_id, range)
        lane += 1
      end
    end
    
    lanes.each_with_index do |lane, i|
      @swimlanes[i] = {} if @swimlanes[i].blank?
      lane.each_with_index do |product_id, lane_index|
        unless product_id.blank?
          @swimlanes[i][product_id] = product = {start: lane.length, end: 0} unless @swimlanes[i].has_key?(product_id)
          @swimlanes[i][product_id][:start] = lane_index if @swimlanes[i][product_id][:start] > lane_index
          @swimlanes[i][product_id][:end] = lane_index if @swimlanes[i][product_id][:end] < lane_index
        end
      end
    end
  end

  def next_lane lanes, lane_index, product_id, range
    continue = true
    free = true
    
    lanes[lane_index] = [] if lanes[lane_index].blank?
    lane = lanes[lane_index]

    (range[:start]..range[:end]).each do |i|
      free = lane[i].blank? && free
    end

    if free
      continue = false
      (range[:start]..range[:end]).each do |i|
        lane[i] = product_id
      end
    end

    continue
  end

  def max_length
    max = 0
    @pipeline.each do |product_id, pipe|
      max = pipe.length if pipe.length > max
    end
    return max
  end

  def max_swimlanes
    Resource.count
  end
end
