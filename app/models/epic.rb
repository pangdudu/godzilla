class Epic < ActiveRecord::Base
  attr_accessible :priority, :stake_holders, :title, :description, :product_id, :epic_resources_attributes
  
  belongs_to :product
  
  has_many :epic_resources

  accepts_nested_attributes_for :epic_resources, :allow_destroy => true

  def estimate_for(resource)
    estimate = 0
    self.epic_resources.where(resource_id: resource.id).each do |er|
      estimate += er.time_estimation
    end
    estimate
  end
end
