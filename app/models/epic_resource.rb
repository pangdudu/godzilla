class EpicResource < ActiveRecord::Base
  attr_accessible :epic_id, :resource_id, :time_estimation
  
  belongs_to :epic
  belongs_to :resource

  validates_uniqueness_of :epic_id, scope: :resource_id
end
