class RoadmapController < ApplicationController
  def products
    @products = Product.order(:priority).all
    @total_longest_estimate = 0
    @products.each do |p|
      @total_longest_estimate += p.longest_estimate[:estimate]
    end
  end

  def products_parallel
    pipeline = Pipeline.new
    pipeline.fill
    pipeline.fill_swimlanes
    @swimlanes = pipeline.swimlanes
    @total_longest_estimate = pipeline.max_length
  end
end
