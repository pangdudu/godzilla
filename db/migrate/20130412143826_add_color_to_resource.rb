class AddColorToResource < ActiveRecord::Migration
  def change
    add_column :resources, :color, :string
  end
end
