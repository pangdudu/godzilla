class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.integer :priority
      t.text :vision
      t.text :scope
      t.text :features

      t.timestamps
    end
  end
end
