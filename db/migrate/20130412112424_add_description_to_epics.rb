class AddDescriptionToEpics < ActiveRecord::Migration
  def change
    add_column :epics, :description, :text
  end
end
