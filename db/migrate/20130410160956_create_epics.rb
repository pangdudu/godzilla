class CreateEpics < ActiveRecord::Migration
  def change
    create_table :epics do |t|
      t.string :title
      t.integer :priority
      t.text :stake_holders
      t.references :component
      t.references :product

      t.timestamps
    end
    add_index :epics, :component_id
    add_index :epics, :product_id
  end
end
