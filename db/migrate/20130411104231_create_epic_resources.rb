class CreateEpicResources < ActiveRecord::Migration
  def change
    create_table :epic_resources do |t|
      t.references :epic
      t.references :resource
      t.integer :time_estimation

      t.timestamps
    end
    add_index :epic_resources, :epic_id
    add_index :epic_resources, :resource_id
  end
end
