# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130412143826) do

  create_table "epic_resources", :force => true do |t|
    t.integer  "epic_id"
    t.integer  "resource_id"
    t.integer  "time_estimation"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "epic_resources", ["epic_id"], :name => "index_epic_resources_on_epic_id"
  add_index "epic_resources", ["resource_id"], :name => "index_epic_resources_on_resource_id"

  create_table "epics", :force => true do |t|
    t.string   "title"
    t.integer  "priority"
    t.text     "stake_holders"
    t.integer  "component_id"
    t.integer  "product_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.text     "description"
  end

  add_index "epics", ["component_id"], :name => "index_epics_on_component_id"
  add_index "epics", ["product_id"], :name => "index_epics_on_product_id"

  create_table "products", :force => true do |t|
    t.string   "title"
    t.integer  "priority"
    t.text     "vision"
    t.text     "scope"
    t.text     "features"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "resources", :force => true do |t|
    t.string   "title"
    t.float    "capacity"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "color"
  end

end
